**GENERATION OF PAIRWISE SEQUENCE IDENTITY DOT-PLOTS**
- All code and documentation for the software, StainedGlass, were developed by Vollger et al. 2021 (_StainedGlass: Interactive visualization of massive tandem repeat structures with identity heatmaps._)

The input for this program is a fasta sequence fragmented into windows (1Kbp) after which all possible
pairwise alignments between the fragments are calculated using minimap2. 
The color used in the dot-plot was then determined by the sequence identity of the alignment which was
calculated as: 

$$ ID = 100 \biggl( \frac{M}{M+X+I+D} \biggr) $$

where $ID$ was the percent sequence identity, $M$ the number of matches, $X$ the number of
mismatches, $I$ the number of insertion events, and $D$ the number of deletion events. When
there were multiple alignments between the same two sequence fragments all alignments other
than the one with the most matches were filtered out regardless of their sequence identity. The
resulting matrix of percent identity scores was then visualized using ggplot and geom_tile. 
