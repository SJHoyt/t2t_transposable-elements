**NOTES FOR DATA VISUALIZATION**

**Methylation boxplots, violins, metaplots, heatmaps and phase-methylation plots, all of which were developed by Ariel Gershman:**
- Generation of genome as object for R: https://github.com/timplab/T2T-Epigenetics/tree/main/make_bsgenome_package
- Utils for R: https://github.com/timplab/T2T-Epigenetics/tree/main/utils 

**Transduction plots were modified for this paper by Reza Halabian, Matias Rodriguez, and Wojciech Makalowski**

**CASK visualizations were developed by Charles Limouse**
