##Generation of PROseq DEEPtools promoter profile and heatmap for split sense-antisense
# plus strand regions; minus (antisense), plus (sense), total bigwigs
computeMatrix scale-regions -p 12 \
-R <REGIONS_PLUS_STRAND> \
-S <MINUS_BIGWIG> <PLUS_BIGWIG> <TOTAL_BIGWIG> \
-o <OUT_PLUS.mat.gz> \
-m 1000 -b 100 -a 100 -bs 10 \
--averageTypeBins max \
--missingDataAsZero
# minus strand regions; plus (antisense), minus (sense), total bigwigs
computeMatrix scale-regions -p 12 \
-R <REGIONS_MINUS_STRAND> \
-S <PLUS_BIGWIG> <MINUS_BIGWIG> <TOTAL_BIGWIG> \
-o <OUT_MINUS.mat.gz> \
-m 1000 -b 100 -a 100 -bs 10 \
--averageTypeBins max \
--missingDataAsZero
# combine plus and minus strand region matrices
computeMatrixOperations rbind -m <OUT_PLUS.mat.gz> <OUT_MINUS.mat.gz> \
-o <OUT.mat.gz>
  # create heatmap
  plotHeatmap -m <OUT.mat.gz> \
-o <OUT.htmp.pdf> \
--dpi 800 \
--colorMap Blues Reds Purples \
--averageTypeSummaryPlot mean \
--samplesLabel "Antisense" "Sense" "Total" \
--heatmapHeight 15 --heatmapWidth 4 \
--zMin 0 --yMin 0 \
--zMax <zMAX> --yMax <yMAX> \
--outFileSortedRegions <OUT.htmpSorted.bed>
