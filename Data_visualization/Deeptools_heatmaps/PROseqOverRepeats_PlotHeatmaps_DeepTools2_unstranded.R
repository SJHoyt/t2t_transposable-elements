computeMatrix scale-regions -p 10 \
-R ${regions} \
-S ${bigWigs} \
-o ${OUTPUT}.mat.gz \
-m 1000 -b 100 -a 100 -bs 10 \
--averageTypeBins max \
--missingDataAsZero

plotHeatmap -m ${OUTPUT}.mat.gz \
-o ${OUTPUT}.htmp.pdf \
--dpi 800 \
--colorMap Reds \
--averageTypeSummaryPlot mean \
--samplesLabel ${samples} \
--regionsLabel "${nElems} ${elem} elements" \
--heatmapHeight 15 --heatmapWidth 4 \
--zMin 0 --yMin 0 \
--zMax ${zMax} --yMax ${yMax} \
--outFileSortedRegions ${OUTPUT}_regions_htmpSorted.bed
