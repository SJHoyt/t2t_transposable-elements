#DeepTools profiles of averaged unique 21mer coverage over two _AluY_ groups with and standard error (SE) included. 
#Since _Alu_ elements tend to be truncated from the 5' end, it makes the most sense to fixed all elements to the 3' end 
#and since they are ~350bp full-length, this code will plot the signal 500bp (-b) from the fixed 3' end into the element. 
#All elements are sorted vertically in the heatmap by length (--sortUsing region_length; top: longest; bottom: shortest). 

computeMatrix reference-point --referencePoint TES -p 12 \
-R AluY_HOTnot_CHM13ABpool_anti-sense_htmpSorted_5ksubsamp_HOTonly.bed AluY_HOTnot_CHM13ABpool_anti-sense_htmpSorted_5ksubsamp_notonly.bed \
-S chm13v1.0.single.k21.bigwig \
-o AluY_chm13v1.0_HOTnot_chm13v1.0.single.k21_OUT.mat.gz \
-b 500 -a 100 -bs 10 \
--averageTypeBins max \
--missingDataAsZero
#relabel matrix:
computeMatrixOperations relabel -m AluY_chm13v1.0_HOTnot_chm13v1.0.single.k21_OUT.mat.gz -o AluY_chm13v1.0_HOTnot_chm13v1.0.single.k21_OUT_rename.mat.gz --groupLabels "AluY_HOT" "AluY_not"
#create heatmap
plotHeatmap -m AluY_chm13v1.0_HOTnot_chm13v1.0.single.k21_OUT_rename.mat.gz --sortUsing region_length \
-o /core/labs/Oneill/T2T_working_space/chm13v1.1_revised/deeptools/Alu/AluY_chm13v1.0_HOTnot_chm13v1.0.single.k21_SE_OUT.htmp.pdf \
--dpi 800 --plotType se \
--colorMap Greys \
--colorList white,lightgrey \
--averageTypeSummaryPlot mean \
--samplesLabel "unique_21mer" \
--heatmapHeight 15 --heatmapWidth 4 \
--zMin 0 --yMin 0
