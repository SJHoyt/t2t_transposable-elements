========> Linkage graphs (Fig 3a)

# The following shows the content of karyotype_dual_comparison.tsv which was used for the linkage graph. Some parts of this file have been created manually, especially the 4th column. (NOTE: In rare cases, based on the R version or platform used, you may need to change the chromosome X to 23)

Chr	Start	End	fill	species	size	color
1	1	248387497	556b2f	Progenitor	12	252525
2	1	242696747	8b4513	Progenitor	12	252525
3	1	201106605	191970	Progenitor	12	252525
4	1	193575430	008000	Progenitor	12	252525
5	1	182045437	008b8b	Progenitor	12	252525
6	1	172126870	9acd32	Progenitor	12	252525
7	1	160567423	8b008b	Progenitor	12	252525
8	1	146259322	ff4500	Progenitor	12	252525
9	1	150617274	ffa500	Progenitor	12	252525
10	1	134758122	ffff00	Progenitor	12	252525
11	1	135127772	7cfc00	Progenitor	12	252525
12	1	133324781	00fa9a	Progenitor	12	252525
13	1	114240146	dc143c	Progenitor	12	252525
14	1	101219177	00ffff	Progenitor	12	252525
15	1	100338308	00bfff	Progenitor	12	252525
16	1	96330493	0000ff	Progenitor	12	252525
17	1	84277185	ff00ff	Progenitor	12	252525
18	1	80542536	db7093	Progenitor	12	252525
19	1	61707359	f0e68c	Progenitor	12	252525
20	1	66210247	ff1493	Progenitor	12	252525
21	1	45827691	7b68ee	Progenitor	12	252525
22	1	51353906	ffa07a	Progenitor	12	252525
X	1	154259625	ee82ee	Progenitor	12	252525
1	1	248387497	969696	Offspring	12	252525
2	1	242696747	969696	Offspring	12	252525
3	1	201106605	969696	Offspring	12	252525
4	1	193575430	969696	Offspring	12	252525
5	1	182045437	969696	Offspring	12	252525
6	1	172126870	969696	Offspring	12	252525
7	1	160567423	969696	Offspring	12	252525
8	1	146259322	969696	Offspring	12	252525
9	1	150617274	969696	Offspring	12	252525
10	1	134758122	969696	Offspring	12	252525
11	1	135127772	969696	Offspring	12	252525
12	1	133324781	969696	Offspring	12	252525
13	1	114240146	969696	Offspring	12	252525
14	1	101219177	969696	Offspring	12	252525
15	1	100338308	969696	Offspring	12	252525
16	1	96330493	969696	Offspring	12	252525
17	1	84277185	969696	Offspring	12	252525
18	1	80542536	969696	Offspring	12	252525
19	1	61707359	969696	Offspring	12	252525
20	1	66210247	969696	Offspring	12	252525
21	1	45827691	969696	Offspring	12	252525
22	1	51353906	969696	Offspring	12	252525
X	1	154259625	969696	Offspring	12	252525


A) Linkage graph for L1


cat LEVEL4a.L1.transd.chm13 LEVEL4b.L1.transd.chm13 | sed "s/ //g" | awk '{print $8 "\t" $1 "\t" $2 "\t" $3}' | sed "s/-/\t/" | awk -F "\t" '{split($1,a,":"); split($2,b,":"); if ($2 ~/-/) print a[2] "\t" b[1]-($5-$4) "\t" b[1] "\t" $3 "\t" $4 "\t" $5 "\t" a[2]"_BBB"; else print a[2] "\t" a[3] "\t" a[3]+($5-$4) "\t" $3 "\t" $4 "\t" $5 "\t" a[2]"_BBB"}' | sort -V -k1,1 -k2,2 | sed -f sed.script.to_modify_chr.color  > L1.temp

for f in  `tail -n +2 L1.temp | cut -f1 | sort -V | uniq -c | sort -nr | awk '{print $2}'` ; do awk -v my_var="$f" '$1 == my_var {print $0}' L1.temp ; done | awk 'BEGIN {print "Species_1\tStart_1\tEnd_1\tSpecies_2\tStart_2\tEnd_2\tfill"}1' > L1.linkage.R.tsv

# R codes

library("RIdeogram")
CHR <- read.table("karyotype_dual_comparison.tsv", sep = "\t", header = T, stringsAsFactors = F)
L1_link <- read.table("L1.linkage.R.tsv", sep = "\t", header = T, stringsAsFactors = F)
ideogram(karyotype = CHR, synteny = L1_link)



B) Linkage graph for Alu

cat LEVEL4a.ALU.transd.chm13 LEVEL4b.ALU.transd.chm13 | sed "s/ //g" | awk '{print $8 "\t" $1 "\t" $2 "\t" $3}' | sed "s/-/\t/" | awk -F "\t" '{split($1,a,":"); split($2,b,":"); if ($2 ~/-/) print a[2] "\t" b[1]-($5-$4) "\t" b[1] "\t" $3 "\t" $4 "\t" $5 "\t" a[2]"_BBB"; else print a[2] "\t" a[3] "\t" a[3]+($5-$4) "\t" $3 "\t" $4 "\t" $5 "\t" a[2]"_BBB"}' | sort -V -k1,1 -k2,2 sed -f sed.script.to_modify_chr.color  > Alu.temp

for f in  `tail -n +2 Alu.temp | cut -f1 | sort -V | uniq -c | sort -nr | awk '{print $2}'` ; do awk -v my_var="$f" '$1 == my_var {print $0}' L1.temp ; done | awk 'BEGIN {print "Species_1\tStart_1\tEnd_1\tSpecies_2\tStart_2\tEnd_2\tfill"}1' > Alu.linkage.R.tsv

# R codes

library("RIdeogram")
CHR <- read.table("karyotype_dual_comparison.tsv", sep = "\t", header = T, stringsAsFactors = F)
Alu_link <- read.table("Alu.linkage.R.tsv", sep = "\t", header = T, stringsAsFactors = F)
ideogram(karyotype = CHR, synteny = Alu_link)





C) Linkage graph for SVA 3’


cat LEVEL4a.SVA.3pr.transd.chm13 LEVEL4b.SVA.3pr.transd.chm13 | sed "s/ //g" | awk '{print $8 "\t" $1 "\t" $2 "\t" $3}' | sed "s/-/\t/" | awk -F "\t" '{split($1,a,":"); split($2,b,":"); if ($2 ~/-/) print a[2] "\t" b[1]-($5-$4) "\t" b[1] "\t" $3 "\t" $4 "\t" $5 "\t" a[2]"_BBB"; else print a[2] "\t" a[3] "\t" a[3]+($5-$4) "\t" $3 "\t" $4 "\t" $5 "\t" a[2]"_BBB"}' | sort -V -k1,1 -k2,2 | sed -f sed.script.to_modify_chr.color  > SVA.3pr.temp

for f in  `tail -n +2 SVA.3pr.temp | cut -f1 | sort -V | uniq -c | sort -nr | awk '{print $2}'` ; do awk -v my_var="$f" '$1 == my_var {print $0}' L1.temp ; done | awk 'BEGIN {print "Species_1\tStart_1\tEnd_1\tSpecies_2\tStart_2\tEnd_2\tfill"}1' > SVA.3pr.linkage.R.tsv


# R codes

library("RIdeogram")
CHR <- read.table("karyotype_dual_comparison.tsv", sep = "\t", header = T, stringsAsFactors = F)
SVA_3pr_link <- read.table("SVA_3pr_link", sep = "\t", header = T, stringsAsFactors = F)
ideogram(karyotype = CHR, synteny = SVA_3pr_link)



D) Linkage graph for SVA 5'

cat LEVEL4a.SVA.5pr.transd.chm13 LEVEL4b.SVA.5pr.transd.chm13 | sed "s/ //g" | awk '{print $8 "\t" $1 "\t" $2 "\t" $3}' | sed "s/-/\t/" | awk -F "\t" '{split($1,a,":"); split($2,b,":"); if ($2 ~ /\+/) print a[2] "\t" b[1]-($5-$4) "\t" b[1] "\t" $3 "\t" $4 "\t" $5 "\t" a[2]"_BBB"; else print a[2] "\t" a[3] "\t" a[3]+($5-$4) "\t" $3 "\t" $4 "\t" $5 "\t" a[2]"_BBB"}' | sort -V -k1,1 -k2,2 | sed -f sed.script.to_modify_chr.color  > SVA.5pr.temp


for f in  `tail -n +2 SVA.5pr.temp | cut -f1 | sort -V | uniq -c | sort -nr | awk '{print $2}'` ; do awk -v my_var="$f" '$1 == my_var {print $0}' L1.temp ; done | awk 'BEGIN {print "Species_1\tStart_1\tEnd_1\tSpecies_2\tStart_2\tEnd_2\tfill"}1' > SVA.5pr.linkage.R.tsv


# R codes

library("RIdeogram")
CHR <- read.table("karyotype_dual_comparison.tsv", sep = "\t", header = T, stringsAsFactors = F)
SVA_5pr_link <- read.table("SVA_5pr_link", sep = "\t", header = T, stringsAsFactors = F)
ideogram(karyotype = CHR, synteny = SVA_5pr_link)



# The following shows the content of the “sed.script.to_modify_chr.color” file used in the above commands.

s/chr1_BBB/556b2f/g
s/chr2_BBB/8b4513/g
s/chr3_BBB/191970/g
s/chr4_BBB/008000/g
s/chr5_BBB/008b8b/g
s/chr6_BBB/9acd32/g
s/chr7_BBB/8b008b/g
s/chr8_BBB/ff4500/g
s/chr9_BBB/ffa500/g
s/chr10_BBB/ffff00/g
s/chr11_BBB/7cfc00/g
s/chr12_BBB/00fa9a/g
s/chr13_BBB/dc143c/g
s/chr14_BBB/00ffff/g
s/chr15_BBB/00bfff/g
s/chr16_BBB/0000ff/g
s/chr17_BBB/ff00ff/g
s/chr18_BBB/db7093/g
s/chr19_BBB/f0e68c/g
s/chr20_BBB/ff1493/g
s/chr21_BBB/7b68ee/g
s/chr22_BBB/ffa07a/g
s/chrX_BBB/ee82ee/g




========> Linkage graph (Fig. S16)


A) L1

cat L1.LEVEL4a.gt.90 L1.LEVEL4b.gt.90 | sed "s/ //g" | awk '{print $8 "\t" $1 "\t" $2 "\t" $3}' | sed "s/-/\t/" | awk -F "\t" '{split($1,a,":"); split($2,b,":"); if ($2 ~/-/) print a[2] "\t" b[1]-($5-$4) "\t" b[1] "\t" $3 "\t" $4 "\t" $5 "\t" "c73d1f"; else print a[2] "\t" a[3] "\t" a[3]+($5-$4) "\t" $3 "\t" $4 "\t" $5 "\t" "c73d1f"}' | sort -V -k1,1 -k2,2 | awk 'BEGIN {print "Species_1\tStart_1\tEnd_1\tSpecies_2\tStart_2\tEnd_2\tfill"}1' > L1.gt.90.R.tsv 

# R
# NOTE; the preparation of karyotype_dual_comparison.tsv has been elaborated above. 

library("RIdeogram")
CHR <- read.table("karyotype_dual_comparison.tsv", sep = "\t", header = T, stringsAsFactors = F)
L1_link_90 <- read.table("L1.gt.90.R.tsv", sep = "\t", header = T, stringsAsFactors = F)
ideogram(karyotype = CHR, synteny = L1_link_90)





B) ALU

cat ALU.LEVEL4a.gt.90 ALU.LEVEL4b.gt.90 | sed "s/ //g" | awk '{print $8 "\t" $1 "\t" $2 "\t" $3}' | sed "s/-/\t/" | awk -F "\t" '{split($1,a,":"); split($2,b,":"); if ($2 ~/-/) print a[2] "\t" b[1]-($5-$4) "\t" b[1] "\t" $3 "\t" $4 "\t" $5 "\t" "c73d1f"; else print a[2] "\t" a[3] "\t" a[3]+($5-$4) "\t" $3 "\t" $4 "\t" $5 "\t" "c73d1f"}' | sort -V -k1,1 -k2,2 | awk 'BEGIN {print "Species_1\tStart_1\tEnd_1\tSpecies_2\tStart_2\tEnd_2\tfill"}1' > ALU.gt.90.R.tsv 

# R
# NOTE; the preparation of karyotype_dual_comparison.tsv has been elaborated above. 

library("RIdeogram")
CHR <- read.table("karyotype_dual_comparison.tsv", sep = "\t", header = T, stringsAsFactors = F)
ALU_link_90 <- read.table("ALU.gt.90.R.tsv", sep = "\t", header = T, stringsAsFactors = F)
ideogram(karyotype = CHR, synteny = ALU_link_90)



C) SVA 3pr

cat SVA.3pr.LEVEL4a.gt.90 SVA.3pr.LEVEL4b.gt.90 | sed "s/ //g" | awk '{print $8 "\t" $1 "\t" $2 "\t" $3}' | sed "s/-/\t/" | awk -F "\t" '{split($1,a,":"); split($2,b,":"); if ($2 ~/-/) print a[2] "\t" b[1]-($5-$4) "\t" b[1] "\t" $3 "\t" $4 "\t" $5 "\t" "c73d1f"; else print a[2] "\t" a[3] "\t" a[3]+($5-$4) "\t" $3 "\t" $4 "\t" $5 "\t" "c73d1f"}' | sort -V -k1,1 -k2,2 | awk 'BEGIN {print "Species_1\tStart_1\tEnd_1\tSpecies_2\tStart_2\tEnd_2\tfill"}1' > SVA.3pr.gt.90.R.tsv 

# R
# NOTE; the preparation of karyotype_dual_comparison.tsv has been elaborated above. 

library("RIdeogram")
CHR <- read.table("karyotype_dual_comparison.tsv", sep = "\t", header = T, stringsAsFactors = F)
SVA.3pr_link_90 <- read.table("SVA.3pr.gt.90.R.tsv", sep = "\t", header = T, stringsAsFactors = F)
ideogram(karyotype = CHR, synteny = SVA.3pr_link_90)




D) SVA 5pr

cat SVA.5pr.LEVEL4a.gt.90 SVA.5pr.LEVEL4b.gt.90 | sed "s/ //g" | awk '{print $8 "\t" $1 "\t" $2 "\t" $3}' | sed "s/-/\t/" | awk -F "\t" '{split($1,a,":"); split($2,b,":"); if ($2 ~/+/) print a[2] "\t" b[1]-($5-$4) "\t" b[1] "\t" $3 "\t" $4 "\t" $5 "\t" "c73d1f"; else print a[2] "\t" a[3] "\t" a[3]+($5-$4) "\t" $3 "\t" $4 "\t" $5 "\t" "c73d1f"}' | sort -V -k1,1 -k2,2 | awk 'BEGIN {print "Species_1\tStart_1\tEnd_1\tSpecies_2\tStart_2\tEnd_2\tfill"}1' > SVA.5pr.gt.90.R.tsv 

# R
# NOTE; the preparation of karyotype_dual_comparison.tsv has been elaborated above. 

library("RIdeogram")
CHR <- read.table("karyotype_dual_comparison.tsv", sep = "\t", header = T, stringsAsFactors = F)
SVA.5pr_link_90 <- read.table("SVA.5pr.gt.90.R.tsv", sep = "\t", header = T, stringsAsFactors = F)
ideogram(karyotype = CHR, synteny = SVA.5pr_link_90)




