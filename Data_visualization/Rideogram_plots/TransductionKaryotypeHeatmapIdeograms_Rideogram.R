========> karyotype-heatmap graphs

# Preparation of the required files

A) T2T karyotype

seqtk comp chm13.draft_v1.0.fasta.gz | awk '{print $1 "\t" "1" "\t" $2}' | grep -v "chrM" > t2t.chrs.length.tsv
grep "acen"  chm13.CytoBandIdeo.v2.txt | sort -V -k1,1  | cut -f1-3 | awk 'BEGIN {lc = 0} {lc ++ ; if ((lc % 2) == 1) printf $2; else print "\t"$3}' > cent.chm13.tsv
paste t2t.chrs.length.tsv cent.chm13.tsv | awk 'BEGIN {print "Chr\tstart\tEnd\tCE_start\tCE_end"}1' | sed "s/chr//g" > RIdiogram.t2t.cent


B) Gene density file
tail -n +2 RIdiogram.t2t.cent | cut -f1,3 > t2t.genome.txt 
bedtools makewindows -g t2t.genome.txt -w 1000000 > genome.windows.bed
bedtools intersect -a genome.windows.bed -b CHM13.gff3.gz -c | sed 's/chr//g' | awk 'BEGIN {print "Chr\tstart\tEnd\tValue"}1' > genome.windows.gene.density.tsv



C) List of offsprings

cat LEVEL4a.L1.transd.chm13 LEVEL4b.L1.transd.chm13 | awk '{print "L1 3pr transduction" "\t" "triangle" "\t" $1 "\t" $2 "\t" $3 "\t" "33a02c"}' > L1.R.offspring.tsv
cat LEVEL4a.ALU.transd.chm13 LEVEL4b.ALU.transd.chm13 | awk '{print "Alu 3pr transduction" "\t" "triangle" "\t" $1 "\t" $2 "\t" $3 "\t" "6a3d9a"}' > ALU.R.offspring.tsv
cat LEVEL4a.SVA.3pr.transd.chm13 LEVEL4b.SVA.3pr.transd.chm13 | awk '{print "SVA 3pr transduction" "\t" "triangle" "\t" $1 "\t" $2 "\t" $3 "\t" "ff7f00"}' > SVA.3pr.R.offspring.tsv
cat LEVEL4a.SVA.5pr.transd.chm13 LEVEL4b.SVA.5pr.transd.chm13 |  awk '{print "SVA 5pr transduction" "\t" "triangle" "\t" $1 "\t" $2 "\t" $3 "\t" "92282a"}' > SVA.5pr.R.offspring.tsv
cat ALU.R.offspring.tsv L1.R.offspring.tsv SVA.3pr.R.offspring.tsv SVA.5pr.R.offspring.tsv | sed 's/chr//g' | awk 'BEGIN {print "Type\tShape\tChr\tStart\tEnd\tcolor"}1' > merged.offsprings.R.tsv


D) List of progenitors 

cat LEVEL4a.L1.transd.chm13 LEVEL4b.L1.transd.chm13 | sed "s/ //g" | awk '{print $3-$2 "\t" $8}' | sed "s/:/\t/g" | awk '{split($4,a,"-"); if ($6 == "+") print $3 "\t" a[1] "\t" a[1]+$1 "\t" $6; else print $3 "\t" a[2]-$1 "\t" a[2] "\t" $6}' | sort -V -k1,1 -k2,2 | grep "+" | sort -V -k1,1 -k2,2 | uniq -f2 > L1.plus
cat LEVEL4a.L1.transd.chm13 LEVEL4b.L1.transd.chm13 | sed "s/ //g" | awk '{print $3-$2 "\t" $8}' | sed "s/:/\t/g" | awk '{split($4,a,"-"); if ($6 == "+") print $3 "\t" a[1] "\t" a[1]+$1 "\t" $6; else print $3 "\t" a[2]-$1 "\t" a[2] "\t" $6}' | sort -V -k1,1 -k2,2 | grep "-" | awk '{print $0 "\t" $2}' | sort -V -k1,1 -k2,2 | uniq -f3 | cut -f1-4 > L1.minus
cat L1.minus L1.plus | sort -V -k1,1 -k2,2 | awk '{print "L1 3pr progenitor" "\t" "triangle" "\t" $1 "\t" $2 "\t" $3 "\t" "33a02c"}' | sed "s/chr//g" > sources.L1.R.tsv 


cat LEVEL4a.ALU.transd.chm13 LEVEL4b.ALU.transd.chm13 | sed "s/ //g" | awk '{print $3-$2 "\t" $8}' | sed "s/:/\t/g" | awk '{split($4,a,"-"); if ($6 == "+") print $3 "\t" a[1] "\t" a[1]+$1 "\t" $6; else print $3 "\t" a[2]-$1 "\t" a[2] "\t" $6}' | sort -V -k1,1 -k2,2 | grep "+" | sort -V -k1,1 -k2,2 | uniq -f2 > ALU.plus
cat LEVEL4a.ALU.transd.chm13 LEVEL4b.ALU.transd.chm13 | sed "s/ //g" | awk '{print $3-$2 "\t" $8}' | sed "s/:/\t/g" | awk '{split($4,a,"-"); if ($6 == "+") print $3 "\t" a[1] "\t" a[1]+$1 "\t" $6; else print $3 "\t" a[2]-$1 "\t" a[2] "\t" $6}' | sort -V -k1,1 -k2,2 | grep "-" | awk '{print $0 "\t" $2}' | sort -V -k1,1 -k2,2 | uniq -f3 | cut -f1-4 > ALU.minus
cat ALU.minus ALU.plus | sort -V -k1,1 -k2,2 | awk '{print "Alu 3pr progenitor" "\t" "triangle" "\t" $1 "\t" $2 "\t" $3 "\t" "6a3d9a"}' | sed "s/chr//g" > sources.ALU.R.tsv


cat LEVEL4a.SVA.3pr.transd.chm13 LEVEL4b.SVA.3pr.transd.chm13 | sed "s/ //g" | awk '{print $3-$2 "\t" $8}' | sed "s/:/\t/g" | awk '{split($4,a,"-"); if ($6 == "+") print $3 "\t" a[1] "\t" a[1]+$1 "\t" $6; else print $3 "\t" a[2]-$1 "\t" a[2] "\t" $6}' | sort -V -k1,1 -k2,2 | grep "+" | sort -V -k1,1 -k2,2 | uniq -f2 > SVA.3pr.plus
cat LEVEL4a.SVA.3pr.transd.chm13 LEVEL4b.SVA.3pr.transd.chm13 | sed "s/ //g" | awk '{print $3-$2 "\t" $8}' | sed "s/:/\t/g" | awk '{split($4,a,"-"); if ($6 == "+") print $3 "\t" a[1] "\t" a[1]+$1 "\t" $6; else print $3 "\t" a[2]-$1 "\t" a[2] "\t" $6}' | sort -V -k1,1 -k2,2 | grep "-" | awk '{print $0 "\t" $2}' | sort -V -k1,1 -k2,2 | uniq -f3 | cut -f1-4 > SVA.3pr.minus
cat SVA.3pr.minus SVA.3pr.plus | sort -V -k1,1 -k2,2 |  awk '{print "SVA 3pr progenitor" "\t" "triangle" "\t" $1 "\t" $2 "\t" $3 "\t" "ff7f00"}' | sed "s/chr//g" > sources.SVA.3pr.R.tsv


cat LEVEL4a.SVA.5pr.transd.chm13 LEVEL4b.SVA.5pr.transd.chm13 | sed "s/ //g" | awk '{print $3-$2 "\t" $8}' | sed "s/:/\t/g" | awk '{split($4,a,"-"); if ($6 == "-") print $3 "\t" a[1] "\t" a[1]+$1 "\t" $6; else print $3 "\t" a[2]-$1 "\t" a[2] "\t" $6}' | sort -V -k1,1 -k2,2 | grep "+" | sort -V -k1,1 -k2,2 | uniq -f2 > SVA.5pr.plus
cat LEVEL4a.SVA.5pr.transd.chm13 LEVEL4b.SVA.5pr.transd.chm13 | awk '{split($4,a,"-"); if ($6 == "-") print $3 "\t" a[1] "\t" a[1]+$1 "\t" $6; else print $3 "\t" a[2]-$1 "\t" a[2] "\t" $6}' | sort -V -k1,1 -k2,2 | grep "-" | awk '{print $0 "\t" $2}' | sort -V -k1,1 -k2,2 | uniq -f3 | cut -f1-4 > SVA.5pr.minus
cat SVA.5pr.minus SVA.5pr.plus| sort -V -k1,1 -k2,2 | awk '{print "SVA 5pr progenitor" "\t" "triangle" "\t" $1 "\t" $2 "\t" $3 "\t" "92282a"}' | sed "s/chr//g" > sources.SVA.5pr.R.tsv


cat sources.ALU.R.tsv sources.L1.R.tsv sources.SVA.3pr.R.tsv sources.SVA.5pr.R.tsv | awk 'BEGIN {print "Type\tShape\tChr\tStart\tEnd\tcolor"}1' > merged.sources.R.tsv




# Producing karyotype-heatmap graph for the offsprings

library("RIdeogram")
gene_density <- read.table("genome.windows.gene.density.tsv", sep = "\t" , header = T , stringsAsFactors = F)
t2t_karyotype <- read.table("RIdiogram.t2t.cent", sep = "\t", header = T, stringsAsFactors = F)
transductions <- read.table("merged.offsprings.R.tsv", sep = "\t", header = T, stringsAsFactors = F)
ideogram(karyotype = t2t_karyotype, overlaid = gene_density, label = transductions, label_type = "marker")


# Producing karyotype-heatmap graph for the progenitors

transd_sources <- read.table("merged.sources.R.t”sv, sep = "\t", header = T, stringsAsFactors = F)
ideogram(karyotype = t2t_karyotype, label = transd_sources, label_type = "marker")

NOTE; variable of t2t_karyotype has been created in the previous step!





 










