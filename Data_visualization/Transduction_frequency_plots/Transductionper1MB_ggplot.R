library(ggplot2)
library(gridExtra)
library(reshape2)

#Load file with distribution of TDs
td <- read.table("chm13.all.TD", sep = " ", head = T)

#Separate data for each chromosome and convert data frames
bp_chr1 <- melt(td[td$chromo == "chr1", 2:6], id.var = "pos")
bp_chr2 <- melt(td[td$chromo == "chr2", 2:6], id.var = "pos")
bp_chr3 <- melt(td[td$chromo == "chr3", 2:6], id.var = "pos")
bp_chr4 <- melt(td[td$chromo == "chr4", 2:6], id.var = "pos")
bp_chr5 <- melt(td[td$chromo == "chr5", 2:6], id.var = "pos")
bp_chr6 <- melt(td[td$chromo == "chr6", 2:6], id.var = "pos")
bp_chr7 <- melt(td[td$chromo == "chr7", 2:6], id.var = "pos")
bp_chr8 <- melt(td[td$chromo == "chr8", 2:6], id.var = "pos")
bp_chr9 <- melt(td[td$chromo == "chr9", 2:6], id.var = "pos")
bp_chr10 <- melt(td[td$chromo == "chr10", 2:6], id.var = "pos")
bp_chr11 <- melt(td[td$chromo == "chr11", 2:6], id.var = "pos")
bp_chr12 <- melt(td[td$chromo == "chr12", 2:6], id.var = "pos")
bp_chr13 <- melt(td[td$chromo == "chr13", 2:6], id.var = "pos")
bp_chr14 <- melt(td[td$chromo == "chr14", 2:6], id.var = "pos")
bp_chr15 <- melt(td[td$chromo == "chr15", 2:6], id.var = "pos")
bp_chr16 <- melt(td[td$chromo == "chr16", 2:6], id.var = "pos")
bp_chr17 <- melt(td[td$chromo == "chr17", 2:6], id.var = "pos")
bp_chr18 <- melt(td[td$chromo == "chr18", 2:6], id.var = "pos")
bp_chr19 <- melt(td[td$chromo == "chr19", 2:6], id.var = "pos")
bp_chr20 <- melt(td[td$chromo == "chr20", 2:6], id.var = "pos")
bp_chr21 <- melt(td[td$chromo == "chr21", 2:6], id.var = "pos")
bp_chr22 <- melt(td[td$chromo == "chr22", 2:6], id.var = "pos")
bp_chrX <- melt(td[td$chromo == "chrX", 2:6], id.var = "pos")

#Set theme and color scheme
theme_set(theme_gray())
alu_color = "#F8766D"
l1_color = "#7CAE00"
sva3_color = "#00999E"
sva5_color = "#b36fe5"


#Function to build axis on top of main plot
build_axis <- function(tab) {
	m1 <- ggplot(tab, aes(x = pos, y = value, fill = variable)) +
	geom_bar(stat = "identity")+ 
	scale_y_continuous(limits = c(0, 10),  breaks = scales::pretty_breaks(n = 3))+
	scale_x_continuous(breaks = scales::pretty_breaks(n = 20), expand = c(0, 0), limits = c(0, 250), position = "top") +
	theme(legend.position = "none", 
	      axis.title.x = element_blank(),
	      axis.text.x = element_text(size = 6, vjust = -1),
	      axis.title.y = element_blank(),
	      axis.ticks.y = element_blank(),
	      axis.text.y = element_blank(),
	      ) +
	theme(plot.margin = margin(26, 1, 1, 42, "points")) +
	annotate("rect", xmin = 0, xmax = 250, ymin = 0, ymax = 10, alpha = 1 , fill = "white")
	return (m1)
}

#Function that produces a plot for each chromosome
build_plot <- function(tab, chr, len, cen_start, cen_end) {
	chr_plot <- ggplot(tab, aes(x = pos, y = value, fill = variable)) +
	geom_bar(stat = "identity")+ 
	scale_y_continuous(limits = c(0,6.5),  breaks = c(0,3,6), expand = c(0, 0))+
	scale_x_continuous(breaks = scales::pretty_breaks(n = 20), expand = c(0, 0), limits = c(0, 250)) +
	ylab(chr)+
	theme(legend.position = "none", 
	      axis.title.x = element_blank(),
	      axis.text.x = element_blank(),
	      axis.text.y = element_text(size = 6),
	      axis.title.y = element_text(angle = 0, vjust = 0.5, hjust = 1, size = 7),
	      axis.ticks.x = element_blank(),
	      ) +
	scale_fill_manual(values = c(alu_color, l1_color, sva3_color, sva5_color))+
	theme(plot.margin = margin(2, 1, 1, 5, "points")) +
	annotate("rect", xmin = len+1, xmax = 250, ymin = 0, ymax = 6.5, alpha = 1 , fill = "white")+
	annotate("rect", xmin = cen_start, xmax = cen_end, ymin = 0, ymax = 6.5, alpha = 0.15, fill = "black")
	return (chr_plot)
}

#Builds each plot
top_axis <-build_axis(bp_chr1)
p1 <- build_plot(bp_chr1, "Chr1  ", 248, 121, 125)
p2 <- build_plot(bp_chr2, "Chr2  ", 242, 91, 96)
p3 <- build_plot(bp_chr3, "Chr3  ", 201, 87, 96)
p4 <- build_plot(bp_chr4, "Chr4  ", 193, 48, 55)
p5 <- build_plot(bp_chr5, "Chr5  ", 182, 46, 52)
p6 <- build_plot(bp_chr6, "Chr6  ", 172, 58, 63)
p7 <- build_plot(bp_chr7, "Chr7  ", 160, 58, 62)
p8 <- build_plot(bp_chr8, "Chr8  ", 146, 43, 47)
p9 <- build_plot(bp_chr9, "Chr9  ", 150, 43, 47)
p10 <- build_plot(bp_chr10, "Chr10", 134, 38, 41)
p11 <- build_plot(bp_chr11, "Chr11", 135, 51, 55)
p12 <- build_plot(bp_chr12, "Chr12", 133, 33, 37)
p13 <- build_plot(bp_chr13, "Chr13", 114, 16, 18)
p14 <- build_plot(bp_chr14, "Chr14", 101, 16, 18)
p15 <- build_plot(bp_chr15, "Chr15", 100, 17, 20)
p16 <- build_plot(bp_chr16, "Chr16", 96, 35, 38)
p17 <- build_plot(bp_chr17, "Chr17", 84, 23, 28)
p18 <- build_plot(bp_chr18, "Chr18", 80, 15, 21)
p19 <- build_plot(bp_chr19, "Chr19", 61, 24, 30)
p20 <- build_plot(bp_chr20, "Chr20", 66, 25, 30)
p21 <- build_plot(bp_chr21, "Chr21", 45, 10, 12)
p22 <- build_plot(bp_chr22, "Chr22", 51, 13, 17)
px <- build_plot(bp_chrX, "ChrX ", 154, 57, 62)

#Arrange individual plot in same output
final_plot <- grid.arrange(top_axis, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, px, nrow = 24)

#Save SVG file with plot
ggsave("td_counts_bin.v3.svg", plot = final_plot, units = "cm", height = 23, width = 17, dpi = 300)

