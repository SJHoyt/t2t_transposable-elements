---
title: "T2T-TE : CASK and RM data plotting"
author: "CL"
output:
  html_document:
    toc: true 
    toc_depth: 3
    number_sections: true 
    df_print: paged
---


```{r, include=FALSE, cache=FALSE}
library(tidyr)
library(dplyr)
library(ggplot2)
library(arrow)
# library(grid)
library(cowplot)
library(ggrepel)
library(ggsci)
library(egg)
library(here)
library(colorspace)
```

Define figures theme
```{r}
theme_publish <- function(base_size = 12, base_family = "Helvetica") {
  (theme_minimal(base_size = base_size, base_family = base_family)
  + theme(
      axis.title = element_text(size = 14),
      plot.title = element_text(size = 16, hjust = 0.5),
      text = element_text(size = 12, colour = "black"),
      axis.text = element_text(size = 12, , colour = "black"),
      axis.line = element_line(colour = "black"),
      axis.ticks = element_line(colour = "black"),
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      legend.position = "bottom",
      legend.direction = "horizontal",
      panel.grid = element_blank(),
      panel.border = element_blank(),
      legend.title = element_text(size = 14, face = "italic"),
      legend.text = element_text(size = 12),
      strip.text = element_text(size = 16),
      plot.margin = margin(0, 0, 0, 0, "cm"),
      panel.background = element_rect(fill = "transparent", color = NA),
      plot.background = element_rect(fill = "transparent", color = NA),
      legend.margin = margin(-6, 0, 0, 0, "pt"),
      legend.spacing.y = unit(6, "pt")
    ))
}
```



# Load analysis ready data

```{r}
DATA_ROOT <- "Transcriptional_analyses/CASK_analysis/"
cts_class_avg <- read_parquet(here(DATA_ROOT,"output/counts_repeats_diffseries_byclass_repaverage.parquet"))

cts_class_avg_timeseries <- read_parquet(here(DATA_ROOT,"output/counts_repeats_timeseries_byclass_repaverage.parquet"))

counts_kmers <- read_parquet(here(DATA_ROOT,"output/counts_repeats_diffseries_byfamily_repaverage.parquet"))
```


# Family level FPM and enrichement

For the whole differentiation series
```{r}
p <- cts_class_avg %>%
  dplyr::filter(!(celltype %in% c("shuffle")), method == "kmers") %>%
  ggplot(aes(x = type_name, y = FPM, fill = celltype)) +
  geom_col(position = "dodge") +
  geom_errorbar(aes(ymin = FPM, ymax = FPM_ub),
    width = .2,
    position = position_dodge(.9)
  ) +
  theme_publish() +
  ggsci::scale_fill_jco() +
  scale_y_continuous(expand = c(0, 0)) +
  theme(axis.text.x = element_text(angle = 45, hjust = 1), axis.title.x = element_blank())

p_fixed <- egg::set_panel_size(p = p, margin = unit(0.5, "in"), width = unit(0.1 * nrow(p$data) / 2, "in"), height = unit(1.2, "in"))

plot_grid(p_fixed)

# ggsave2(here("figures/FPM_diffseries_byclass.pdf"))
```

CHM13 and RNAseq only
```{r}
p <- cts_class_avg %>%
  dplyr::filter(celltype %in% c("CHM13", "CHM13_RNAseq", "shuffle"), method == "kmers") %>%
  ggplot(aes(x = type_name, y = FPM, fill = celltype)) +
  geom_col(position = "dodge") +
  geom_errorbar(aes(ymin = FPM, ymax = FPM_ub),
    width = .2,
    position = position_dodge(.9)
  ) +
  theme_publish() +
  ggsci::scale_fill_jco() +
  scale_y_continuous(expand = c(0, 0)) +
  theme(axis.text.x = element_text(angle = 45, hjust = 1), axis.title.x = element_blank())

p_fixed <- egg::set_panel_size(p = p, margin = unit(0.5, "in"), width = unit(0.15 * nrow(p$data) / 2, "in"), height = unit(1.2, "in"))
plot_grid(p_fixed)

# ggsave2(here("figures/FPM_CHM13only_byclass.pdf"))
```


## Plot enrichment
```{r}
p <- cts_class_avg %>%
  dplyr::filter(!(celltype %in% c("shuffle")), method == "kmers") %>%
  # dplyr::filter(celltype %in% c('CHM13','CHM13_RNAseq')) %>%
  ggplot(aes(x = type_name, y = log2obsOverExp, fill = celltype)) +
  geom_col(position = "dodge") +
  geom_errorbar(aes(ymin = log2obsOverExp_lb, ymax = log2obsOverExp_ub),
    width = .2,
    position = position_dodge(.9)
  ) +
  # lemon::facet_rep_wrap(~method)+
  theme_publish() +
  ggsci::scale_fill_jco() +
  scale_y_continuous(expand = c(0, 0)) +
  theme(axis.text.x = element_text(angle = 45, hjust = 1), axis.title.x = element_blank())

p_fixed <- egg::set_panel_size(p = p, margin = unit(0.5, "in"), width = unit(0.1 * nrow(p$data) / 2, "in"), height = unit(1.2, "in"))
# set_palette(p_fixed, "jco")
plot_grid(p_fixed)
```

```{r}
p <- cts_class_avg %>%
  dplyr::filter((celltype %in% c("CHM13", "CHM13_RNAseq")), method == "kmers") %>%
  ggplot(aes(x = type_name, y = log2obsOverExp, fill = celltype)) +
  geom_col(position = "dodge") +
  geom_errorbar(aes(ymin = log2obsOverExp_lb, ymax = log2obsOverExp_ub),
    width = .2,
    position = position_dodge(.9)
  ) +
  theme_publish() +
  ggsci::scale_fill_jco() +
  scale_y_continuous(expand = c(0, 0)) +
  theme(axis.text.x = element_text(angle = 45, hjust = 1), axis.title.x = element_blank()) +
  labs(y = "Repeat enrichment in \nreads vs. genome\nLog2(FPM/shuffle)")

p_fixed <- egg::set_panel_size(p = p, margin = unit(0.5, "in"), width = unit(0.2 * nrow(p$data) / 2, "in"), height = unit(1.2, "in"))
plot_grid(p_fixed)

# ggsave2(here("figures/enrichment_CHM13only_byclass.pdf"))
```


# Heatmap of enrichement all cell lines

```{r, fig.height=6}

xx <- bind_rows(cts_class_avg, cts_class_avg_timeseries %>% dplyr::filter(!(celltype %in% c("CHM13", "shuffle"))) %>% mutate(celltype = factor(celltype, levels = c("Async", "Mitosis", "60min", "90min", "2hr", "3hr", "4hr", "6hr", "8hr", "12hr")))) %>%
  dplyr::filter(!(celltype %in% c("shuffle")), method == "kmers") %>%
  # dplyr::filter(celltype %in% c('CHM13','CHM13_RNAseq')) %>%
  dplyr::arrange(log2obsOverExp) %>%
  mutate(type_name = as.character(type_name))

norder <- xx %>%
  dplyr::filter((celltype %in% c("CHM13")), method == "kmers") %>%
  pull(type_name)

p <- xx %>%
  mutate(type_name = factor(type_name, levels = norder)) %>%
  ggplot(aes(y = type_name, x = celltype, fill = log2obsOverExp)) +
  geom_tile() +
  scale_y_discrete(limits = rev(norder)) +
  theme_publish() +
  scale_fill_continuous_diverging("Blue-Red 3", rev = T) +
  scale_x_discrete(position = "top") +
  theme(axis.text.x = element_text(angle = 45, vjust = 0.5, hjust = 0)) +
  coord_equal()



p_fixed <- egg::set_panel_size(p = p, margin = unit(3, "in"), width = unit(xx %>% pull(celltype) %>% unique() %>% length() / 5, "in"), height = unit(xx %>% pull(type_name) %>% unique() %>% length() / 5, "in"))

plot_grid(p_fixed)
# ggsave2(here("figures/enrichment_RANKED_byclass.pdf"))
```


# Expression and enrichments by repeat class and by cell type
```{r}
plot_byclass_noLOG <- function(cts, cl2plot, ntop = 1000) {
  x <- cts %>%
    dplyr::filter(class == cl2plot)

  thisorder <- x %>%
    dplyr::filter(celltype != "shuffle") %>%
    group_by(family) %>%
    summarize(FPM = sum(FPM)) %>%
    arrange(desc(FPM)) %>%
    ungroup() %>%
    slice_head(n = ntop) %>%
    pull(family)

  x <- x %>%
    dplyr::filter(family %in% thisorder) %>%
    mutate(family = factor(family, levels = thisorder))

  p <- x %>% ggplot(aes(x = family, y = FPM, fill = celltype)) +
    geom_col(position = "dodge") +
    geom_errorbar(aes(ymin = FPM, ymax = FPM_ub),
      width = .2,
      position = position_dodge(.9)
    ) +
    theme_publish() +
    labs(title = cl2plot) +
    ggsci::scale_fill_jco() +
    scale_y_continuous(expand = c(0, 0)) +
    theme(axis.text.x = element_text(angle = 45, hjust = 1), axis.title.x = element_blank())
  return(p)
}

plot_byclass_oe <- function(cts, cl2plot, ntop = 1000) {
  x <- cts %>%
    dplyr::filter(class == cl2plot)

  thisorder <- x %>%
    dplyr::filter(celltype != "shuffle") %>%
    group_by(family) %>%
    summarize(FPM = sum(FPM)) %>%
    arrange(desc(FPM)) %>%
    ungroup() %>%
    slice_head(n = ntop) %>%
    pull(family)

  x <- x %>%
    dplyr::filter(family %in% thisorder) %>%
    mutate(family = factor(family, levels = thisorder))

  p <- x %>% ggplot(aes(x = family, y = log2obsOverExp, fill = celltype)) +
    geom_col(position = position_dodge(.9, preserve = "single")) +
    geom_errorbar(aes(ymin = log2obsOverExp_lb, ymax = log2obsOverExp_ub),
      width = .2,
      position = position_dodge(.9, preserve = "single")
    ) +
    theme_publish() +
    labs(title = cl2plot) +
    ggsci::scale_fill_jco() +
    scale_y_continuous(expand = c(0, 0)) +
    theme(axis.text.x = element_text(angle = 45, hjust = 1), axis.title.x = element_blank())


  return(p)
}
```


```{r, fig.width=8}
mycells <- c("CHM13", "RPE1", "ES", "DE", "duodenum", "ileum")
mytypes <- c("LINE", "SINE", "Satellite", "DNA", "LTR")
p <- sapply(mytypes, function(x) {
  plot_byclass_oe(counts_kmers %>%
    dplyr::filter(celltype %in% mycells) %>%
    mutate(celltype = factor(celltype, levels = mycells)), x, ntop = 10)
}, simplify = F, USE.NAMES = T)

plot_grid(plotlist = lapply(p, function(x) egg::set_panel_size(p = x + theme(legend.position = "none"), margin = unit(0.5, "in"), width = unit(0.05 * nrow(x$data), "in"), height = unit(1.5, "in"))))
# ggsave2(here("figures/enrichment_diffseries_byfamily.pdf"))
```
