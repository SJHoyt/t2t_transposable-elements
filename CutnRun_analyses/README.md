**RELEVANT NOTES AND LINKS FOR CUTNRUN KMER FILTERING**

Following mapping of CutnRun data, follow the steps laid out in `KmerFilteringPipeline-CutnRun_JULY042021_SHoyt.txt` to retain only those reads that overlap a unique kmer. 

**Filtering through Meryl unique CHM13v1.0 21mers and 51mers:**
- For marker-assisted alignment filtering, we removed alignments that did not span any "marker" k-mer as defined in https://doi.org/10.1101/2021.07.02.450803 for k=21. Similarly, for k=51, k-mers present once in T2T-CHM13v1.0 and between 28 and 107 in the Illumina reads were used.
- Meryl unique kmer generation code: https://github.com/arangrhie/T2T-Polish/tree/master/marker_assisted 
- Meryl unique CHM13v1.0 21mers and 51mers: https://s3-us-west-2.amazonaws.com/human-pangenomics/index.html?prefix=T2T/CHM13/assemblies/alignments/marker/ 

**Filtering through unique CHM13v1.0 100mers**
- Unique CHM13v1.0 100mer generation code: https://github.com/msauria/T2T_Kmer_Analysis 
- Unique CHM13v1.0 100mers: https://github.com/msauria/T2T_Kmer_Analysis/blob/main/results/chm13v1_100mer_coverage.bed
