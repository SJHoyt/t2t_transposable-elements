****T2T Consortium Transposable Element Team Code Repository****

This repository is for the T2T Consortium preprint titled, _From telomere to telomere: the transcriptional and epigenetic state of human repeat elements_, which can be found here: https://www.biorxiv.org/content/10.1101/2021.07.12.451456v1 

The other T2T Consortium preprints are listed below:
- _The complete sequence of a human genome_: https://www.biorxiv.org/content/10.1101/2021.05.26.445798v1
- _Epigentic patterns in a complete human genome_: https://www.biorxiv.org/content/10.1101/2021.05.26.443420v1 
- _Complete genomic and epigenetic maps of human centromeres_: https://www.biorxiv.org/content/10.1101/2021.07.12.452052v1 
- _Segmental duplications and their variation in a complete human genome_: https://www.biorxiv.org/content/10.1101/2021.05.26.445678v1
- _A complete reference reference genome improves analysis of human genetic variation_: https://www.biorxiv.org/content/10.1101/2021.07.12.452063v1
- _Chasing perfection: validation and polishing strategies for telomere-to-telomere genome assemblies_: https://www.biorxiv.org/content/10.1101/2021.07.02.450803v1 
 
Except where noted, each code block is uploaded as a basic set of commands, but was run on the University of Connecticut HPC cluster under the SLURM management software. This code block is included here:

#!/bin/bash
#SBATCH --job-name=X
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 15
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=100g
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

This block was modified as needed for memory and performance requirements. R code was run in R/4.1.0 on HPC or local RStudio as memory demands required.

Code used from additional parts of the T2T consortium is listed in their respective repositories here:
- RepeatModeler and utils: https://github.com/Dfam-consortium/RepeatModeler
- CASK: https://github.com/straightlab/cask
- DotPlot scripts: https://mrvollger.github.io/StainedGlass/
- RepeatMasker Summarizer: https://github.com/gabriellehartley/CENP-Gibbon-Analysis
- Genome manipulation in R: https://github.com/timplab/T2T-Epigenetics/tree/main/make_bsgenome_package https://github.com/timplab/T2T-Epigenetics/tree/main/utils/
- Meryl-associated code and unique kmer links for CHM13: https://github.com/arangrhie/T2T-Polish/tree/master/marker_assisted/
- Meryl unique CHM13v1.0 21mers and 51mers: https://s3-us-west-2.amazonaws.com/human-pangenomics/index.html?prefix=T2T/CHM13/assemblies/alignments/marker/
- Unique CHM13v1.0 100mer generation code: https://github.com/msauria/T2T_Kmer_Analysis/ 
- Unique CHM13v1.0 100mers: https://github.com/msauria/T2T_Kmer_Analysis/blob/main/results/chm13v1_100mer_coverage.bed 
-Meryl unique 

UCSC CHM13v1.0 assembly hub browser can be accessed here: 
http://genome.ucsc.edu/cgi-bin/hgTracks?genome=t2t-chm13-v1.0&hubUrl=http://t2t.gi.ucsc.edu/chm13/hub/hub.txt

CHM13v1.0 tracks are available here for download, as well as in the assembly hub browser:
- RepeatMaskerV2: http://t2t.gi.ucsc.edu/chm13/dev/t2t-chm13-v1.0/downloads/t2t-chm13-v1.0.rmskV2.bigBed
- RepeatMasterv2 Composites: http://t2t.gi.ucsc.edu/chm13/dev/t2t-chm13-v1.0/downloads/t2t-chm13-v1.0.composite-repeats-singletons-arrays.bed.gz
- RepeatMasterv2 New Satellites: http://t2t.gi.ucsc.edu/chm13/dev/t2t-chm13-v1.0/downloads/t2t-chm13-v1.0.new-satellites-monomers-arrays.bed.gz

Other Downloadable Tracks:
- RepeatMaskerv2 HG002 chrX: http://t2t.gi.ucsc.edu/chm13/dev/HG002-X-v1.0/downloads/HG002-X-v1.0.rmskV2.bigBed
- RepeatMaskerv2 GRCh38 + chrY: http://t2t.gi.ucsc.edu/chm13/dev/GRCh38/downloads/GRCh38-rmskV2.bigBed
