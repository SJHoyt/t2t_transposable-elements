#!/usr/bin/env python3

import sys

with open(sys.argv[1],'r') as out:
    with open(sys.argv[2],'w') as bed:
        for line in out:
            spl = line.split()
            SW = spl[4]
            div = spl[5]
            percDel = spl[6]
            percIns = spl[7]
            queryName = spl[8]
            queryStart = spl[9]
            queryEnd = spl[10]
            queryLeft = spl[11]
            strand = spl[12]
            repeatMatchName = spl[13]
            repeatClass = spl[14]
            consMatchStart = spl[15]
            consMatchEnd = spl[16]
            consMatchLeft = spl[17]
            ID = spl[18]
            bed.write(SW+'\t'+div+'\t'+percDel+'\t'+percIns+'\t'+queryName+'\t'+queryStart+'\t'+queryEnd+'\t'+queryLeft+'\t'+strand+'\t'+repeatMatchName+'\t'+repeatClass+'\t'+consMatchStart+'\t'+consMatchEnd+'\t'+consMatchLeft+'\t'+ID+'\n')
