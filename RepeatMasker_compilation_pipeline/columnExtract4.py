#!/usr/bin/env python3

import sys

with open(sys.argv[1],'r') as infile:
    with open(sys.argv[2],'w') as bed:
        for line in infile:
            spl = line.split()
            chrom = spl[0]
            start = spl[1]
            stop = spl[2]
            title = spl[3]
            bed.write(chrom+'\t'+start+'\t'+stop+'\t'+title+'\n')
