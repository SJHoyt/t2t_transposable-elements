#!/usr/bin/perl -w

use strict;
use warnings; 

my ($hardRM, $RM, $clean) = @ARGV;

open (OUT1, ">family.out");
open (OUT2, ">GAP.out");
open (OUT3, ">hardRM.out");
open (OUT4, ">RM.out");

system "addValue.py $RM $hardRM hardRM_plus.out";

open (hardRM, 'hardRM_plus.out');

while (<hardRM>) {
    print OUT1 if /family/;
    print OUT2 if /GAP/;
    print OUT3;
}

open (RM, $RM);
while (<RM>) {
    print OUT4;
}
close OUT1;
close OUT2;
close OUT3;
close OUT4;
close hardRM;
close RM;

# Filter out any new families with a Smith-Waterman score of 250 or less
system "rm_filter.py family.out family_250filtered.out";
    
# Convert the RM output to a bed file with extra info 
system "rm_to_bed_expanded.py family_250filtered.out family_250filtered.bed";
system "rm_to_bed_expanded.py GAP.out GAP.bed";
system "rm_to_bed_expanded.py family.out family.bed";
system "rm_to_bed_expanded.py hardRM.out hardRM.bed";

# Remove GAP and new entires from the first run of RepeatMasker
system "subtractBed -a hardRM.bed -b GAP.bed -f 1.0 -r -wb > out1";
system "columnExtract4.py out1 hardRM_minus_GAP.bed";
system "subtractBed -a hardRM_minus_GAP.bed -b family.bed -f 1.0 -r -wb > out2";
system "columnExtract4.py out2 hardRM_minus_GAP_family.bed";

# Convert original masked genome to a bed file with additional information
system "rm_to_bed_expanded.py RM.out RM.bed";

# Eliminate all of the entries in the orignal RM that overlap with the new entries that are above the 250
# Smith-Waterman filter
system "subtractBed -a RM.bed -b family_250filtered.bed -A -wao > out3";
system "columnExtract4.py out3 RM_minus_family_250filtered.bed";

# Concatenate all of the GAP, passed new family RM entries, and 
# remainming original RM entries
system "cat hardRM_minus_GAP_family.bed RM_minus_family_250filtered.bed family_250filtered.bed GAP.bed > compilation1.bed";

# Get output into format to add to UCSC browser
system "replaceComp.py compilation1.bed compilation1_RM.out";
system "columnExtractRM.py compilation1_RM.out compilation1_RM_final.out";

# Add UCSC format option
#system "/u1/home/jrosen/code/RepeatMasker/dist/util/RM2Bed.py compilation1_RM2.out";

if (defined $clean) {
    system "rm RM*";
    system "rm family*";
    system "rm GAP*";
    system "rm hard*";
    system "rm out*";
    system "rm compilation1.bed";
    system "rm compilation1_RM.out";
} else {
    print "No file cleanup";
}
