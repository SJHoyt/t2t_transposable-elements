#!/usr/bin/env python3

import sys

with open(sys.argv[1],'r') as ogMask:
    with open(sys.argv[2],'r') as hardMask:
        with open(sys.argv[3],'w') as hardMaskPlus:
            idList = []
            for line in ogMask:
                spl = line.split()
                ID = int(spl[14])
                idList.append(ID)
            maxID = max(idList)
            for line in hardMask:
                spl = line.split()
                newID = (int(spl[14]) + maxID)
                hardMaskPlus.write(spl[0]+'\t'+spl[1]+'\t'+spl[2]+'\t'+spl[3]+'\t'+spl[4]+'\t'+spl[5]+'\t'+spl[6]+'\t'+spl[7]+'\t'+spl[8]+'\t'+spl[9]+'\t'+spl[10]+'\t'+spl[11]+'\t'+spl[12]+'\t'+spl[13]+'\t'+str(newID)+'\n')
