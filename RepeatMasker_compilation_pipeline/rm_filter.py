#!/usr/bin/env python3

#Filter RepeatMasker lines by the SW score in the first column; 
#print lines with SW of greater than 250
#Usage:
# $ rm_filter.py RepeatMasker_output.out 250Filtered_output.out

import sys

with open(sys.argv[1],'r') as rpmsk:
    with open(sys.argv[2],'w') as bed:
        for line in rpmsk:
            spl=line.split()
            SW = spl[0] 
            if int(SW) > 250:
                bed.write(line)
