#!/usr/bin/env python3

import sys

with open(sys.argv[1],'r') as infile:
    with open(sys.argv[2],'w') as ogRM:
        for line in infile:
            replace = line.replace(';','\t')
            ogRM.write(replace)
