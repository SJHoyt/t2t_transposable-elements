module load RAxML

#Rapid Bootstrap Algorithm
raxmlHPC-PTHREADS -T 20 -f a -m GTRGAMMA -p 12345 -x 12345 -k -# 100 -s L1HS_CENs.fasta -n CENs
raxmlHPC-PTHREADS -T 20 -f a -m GTRGAMMA -p 12345 -x 12345 -k -# 100 -s L1HS_HORs.fasta -n HORs
raxmlHPC-PTHREADS -T 20 -f a -m GTRGAMMA -p 12345 -x 12345 -k -# 100 -s L1HS_Hot.fasta -n Hot
raxmlHPC-PTHREADS -T 20 -f a -m GTRGAMMA -p 12345 -x 12345 -k -# 100 -s L1HS_MONs.fasta -n MONs

#raxmlHPC -m GTRCAT -n SST1_CAT -p 12345 -f b -t RAxML_bestTree.SST1_BL -J MR -z RAxML_bootstrap.SST1 -n LongRun_BestMLBootstraps_SST1_2