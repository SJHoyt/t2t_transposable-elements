##This pipeline was used in the initial assessment of CHM13v1.0

RepeatMasker Pipeline Instructions:

Before running this pipeline, you need to run the following programs:
1.	RepeatMasker on genome of interest (without header)
2.	Hard-mask the genome from step1 
3.	Run RepeatMasker on the hard-masked genome from step2 (without header)

The following dependencies are required:
1.	bedtools
2.	rm_filter.py
3.	rm_to_bed_expanded.py
4.	fixRMtoZero.py

# Put GAP and new families in separate files

$ grep family t2t-chm13-v1.0_masked-hard_nohead.fa.rmout > family_entries.out

$ grep GAP t2t-chm13-v1.0_masked-hard_nohead.fa.rmout > GAP_entries.out

# Filter out any new families with a score of 250 or less

$ rm_filter.py family_entries.out family_entries_250filtered.out

# Convert the RM output to a bed file with extra info in the ID

$ rm_to_bed_expanded.py family_entries_250filtered.out family_entries_250filtered.bed

$ rm_to_bed_expanded.py family_entries.out family_entries.bed

$ rm_to_bed_expanded.py GAP_entries.out GAP_entries.bed

$ rm_to_bed_expanded.py t2t-chm13-v1.0_masked-hard_nohead.fa.rmout chm13.bed

# Remove GAP and new entries from the chm13 masked RM

$ subtractBed -a chm13.bed -b GAP_entries.bed -f 1.0 -r -wb > chm13_minus_GAP.bed

$ awk '{print$1"\t"$2"\t"$3"\t"$4}' chm13_minus_GAP.bed > chm13_minus_GAP_fixed.bed

$ subtractBed -a chm13_minus_GAP_fixed.bed -b family_entries.bed -f 1.0 -r -wb > chm13_minus_GAP_new_before_fix.bed

$ awk '{print$1"\t"$2"\t"$3"\t"$4}' chm13_minus_GAP_new_before_fix.bed
 > chm13_minus_GAP_new.bed

# Convert original masked genome to a bed file with additional information

$ rm_to_bed_expanded.py chm13.draft_v1.0_minus38Y_repeatmasker_nohead.out chm13_RM1.bed


# Eliminate all of the entries in the original RM that overlap with the new entries that are above the 250 SW filter

$ subtractBed -a chm13_RM1.bed -b family_entries_250filtered.bed -A -wao > chm13_RM1_remaining_before_fix.bed

$ awk '{print$1"\t"$2"\t"$3"\t"$4}' chm13_RM1_remaining_before_fix.bed > chm13_RM1_remaining.bed

# Concatenate all of the GAP, passed new family RM entries, and remaining original chm13 entries

$ cat chm13_RM1_remaining.bed chm13_minus_GAP_new.bed family_entries_250filtered.bed GAP_entries.bed > RM_compiled.bed

#  Get output into format to add to UCSC browser

$ sed 's/;/\t/g' RM_compiled.bed > RM_compiled_format1.bed

$ awk '{print$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13"\t"$14"\t"$15"\t"$16"\t"$17"\t"$18}' RM_compiled_format1.bed > RM_compiled_format2.bed

$ sed -i 's/\//\t/g' RM_compiled_format2.bed

$ fixRMtoZero.py RM_compiled_format2.bed RM_compiled_UCSCtrack.bed
