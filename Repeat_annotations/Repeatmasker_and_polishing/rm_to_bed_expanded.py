#!/usr/bin/env python3


#Convert RepeatMasker (RM) output to a bedfile, where the name (fourth column) is the 
#entirety of the RM output separated by semicolons
# Usage: $ rm_to_bed_expanded.py RepeatMasker_output.out Repeatmasker_compressed.bed

import sys

with open(sys.argv[1],'r') as rpmsk:
    with open(sys.argv[2],'w') as bed:
        for line in rpmsk:
            spl=line.split()
            bed.write(spl[4]+'\t'+spl[5]+'\t'+spl[6]+'\t'+spl[9]+';'+spl[0]+';'+spl[1]+';'+spl[2]+';'+spl[3]+';'+spl[4]+';'+spl[5]+';'+spl[6]+';'+spl[7]+';'+spl[8]+';'+spl[9]+';'+spl[10]+';'+spl[11]+';'+spl[12]+';'+spl[13]+'\n')
