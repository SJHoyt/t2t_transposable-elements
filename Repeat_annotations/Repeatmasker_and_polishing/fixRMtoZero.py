#!/usr/bin/env python3

#Convert the RepeatMasker output to a zero-based UCSC track

import sys

with open(sys.argv[1],'r') as rpmsk:
    with open(sys.argv[2],'w') as bed:
        for line in rpmsk:
            spl=line.split()
            zeroStart = str(int(spl[5])-1)
            orient = spl[8]
            sequence = spl[4]
            end = spl[6]
            family = spl[9]
            divergence = spl[1]
            score = spl[0]
            if orient == 'C':
                orient == '-'
            if len(spl) == 14:
                if orient == 'C':
                    bed.write(sequence+'\t'+zeroStart+'\t'+end+'\t'+family+'\t'+score+'\t'+'-'+'\t'+spl[10]+'\t'+'undefined'+'\t'+divergence+'\n')
                else:
                    bed.write(sequence+'\t'+zeroStart+'\t'+end+'\t'+family+'\t'+score+'\t'+'+'+'\t'+spl[10]+'\t'+'undefined'+'\t'+divergence+'\n')
            if len(spl) == 15:
                if orient == 'C':
                    bed.write(sequence+'\t'+zeroStart+'\t'+end+'\t'+family+'\t'+score+'\t'+'-'+'\t'+spl[10]+'\t'+spl[11]+'\t'+divergence+'\n')
                else:
                    bed.write(sequence+'\t'+zeroStart+'\t'+end+'\t'+family+'\t'+score+'\t'+'+'+'\t'+spl[10]+'\t'+spl[11]+'\t'+divergence+'\n')
            if len(spl) == 16:
                if orient == 'C':
                    bed.write(sequence+'\t'+zeroStart+'\t'+end+'\t'+family+'\\'+spl[10]+'\t'+score+'\t'+'-'+'\t'+spl[11]+'\t'+spl[12]+'\t'+divergence+'\n')
                else:
                    bed.write(sequence+'\t'+zeroStart+'\t'+end+'\t'+family+'\\'+spl[10]+'\t'+score+'\t'+'+'+'\t'+spl[11]+'\t'+spl[12]+'\t'+divergence+'\n')
