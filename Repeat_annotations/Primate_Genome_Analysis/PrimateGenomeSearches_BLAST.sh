module load blast

makeblastdb -in {Primate_of_Interest}.fasta -out {Primate_of_Interest_DB_Name} -dbtype DNA

blastn -db {Primate_of_Interest_DB_Name} -query {Repeat_of_Interest} -qcov_hsp_perc 85 > output.txt