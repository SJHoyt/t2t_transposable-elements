- RepeatModeler and utils: https://github.com/Dfam-consortium/RepeatModeler 
- RepeatMasker Summarizer: https://github.com/gabriellehartley/CENP-Gibbon-Analysis 

**CHM13v1.0 Tracks**

Available here for download, as well as viewable in the UCSC assembly hub browser (http://genome.ucsc.edu/cgi-bin/hgTracks?genome=t2t-chm13-v1.0&hubUrl=http://t2t.gi.ucsc.edu/chm13/hub/hub.txt):
- RepeatMaskerv2: http://t2t.gi.ucsc.edu/chm13/dev/t2t-chm13-v1.0/downloads/t2t-chm13-v1.0.rmskV2.bigBed 
- RepeatMasterv2 Composites: http://t2t.gi.ucsc.edu/chm13/dev/t2t-chm13-v1.0/downloads/t2t-chm13-v1.0.composite-repeats-singletons-arrays.bed.gz 
- RepeatMasterv2 New Satellites: http://t2t.gi.ucsc.edu/chm13/dev/t2t-chm13-v1.0/downloads/t2t-chm13-v1.0.new-satellites-monomers-arrays.bed.gz 

**Other Downloadable Tracks**

Available here for download:
- RepeatMaskerv2 HG002 chrX: http://t2t.gi.ucsc.edu/chm13/dev/HG002-X-v1.0/downloads/HG002-X-v1.0.rmskV2.bigBed 
- RepeatMaskerv2 GRCh38 + chrY: http://t2t.gi.ucsc.edu/chm13/dev/GRCh38/downloads/GRCh38-rmskV2.bigBed

