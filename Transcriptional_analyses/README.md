**RELEVANT NOTES AND LINKS FOR PROSEQ AND RNASEQ ANALYSES**

**Filtering through Meryl unique CHM13v1.0 21mers and/or 51mers:**

Following mapping of PROseq/RNAseq data, follow the steps laid out in Read-Locus_KmerFilteringPipeline-PROseq-RNAseq.txt for all methods of filtering applied.
All Meryl unique kmer generation code and code for read-filtering can be found here as developed by Arang Rhie: https://github.com/arangrhie/T2T-Polish/tree/master/marker_assisted
For marker-assisted alignment filtering, we removed alignments that did not span any "marker" k-mer as defined in https://www.biorxiv.org/content/10.1101/2021.07.02.450803v1 for k=21. Similarly, for k=51, k-mers present once in T2T-CHM13v1.0 and between 28 and 107 in the Illumina reads were used.

Meryl unique CHM13v1.0 21mers and 51mers: https://s3-us-west-2.amazonaws.com/human-pangenomics/index.html?prefix=T2T/CHM13/assemblies/alignments/marker/

CHM13v1.0 genome file for use with bedGraphToBigWig: chm13v1.0_seqLengths.genome

**CASK: Classification of Ambivalent Sequences using k-mers**

Developed by Charles Limouse 
CASK core code: https://github.com/straightlab/cask
