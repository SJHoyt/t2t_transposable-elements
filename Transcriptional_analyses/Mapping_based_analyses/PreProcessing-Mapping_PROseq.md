**The following steps were applied for pre-processing and mapping:**

    1) Run fastqc on raw fastqs (view html)
    2) Trim raw files using CutAdapt (quality cut-off of 20 and length cut-off of 20nt)
    3) Run fastqc on trimmed fastqs (view html) 
    4) Get the reverse complement of all clipped reads (ONLY DO THIS FOR PROSEQ, NOT GROSEQ!) using fastx toolkit
    5) Map trimmed and reverse complemented reads to Drosophila melanogaster genome to remove spike-ins using Bowtie2; export as sorted bam using samtools
    6) Pull-out only those reads that do NOT map to Drosophila melanogaster genome (aka. human only reads) using samtools
    7) Convert human only bam into fastq file format using samtools 
    8) Map human-only fastq to human genome (CHM13v1.1) using Bowtie2 (with multi-mappers); export as sorted bam using samtools
    9) Convert sorted bam to bed file format using bedtools 

**Run pre-processing and mapping code:**

_Define variables:_
```
    INpath=/path/to/input/directory/
    INfileprefix=SAMPLE1
    DmelanogasterIndex=/shared/databases/alignerIndex/animal/drosophila_melanogaster/current/bowtie2/Drosophila_melanogaster
    HumanCHM13v1.1Index=/shared/databases/alignerIndex/animal/human/t2t/chm13v1.1
    OUTpath=/path/to/output/directory/
```
_Run code:_
```
    fastqc ${INpath}${INfileprefix}.fastq
    cutadapt -q 20 -m 20 -a TGGAATTCTCGGGTGCCAAGG -o ${INpath}${INfileprefix}_trimmed.fastq ${INpath}${INfileprefix}.fastq &&
    fastqc ${INpath}${INfileprefix}_trimmed.fastq
    fastx_reverse_complement -Q33 -v -i ${INpath}${INfileprefix}_trimmed.fastq -o ${INpath}${INfileprefix}_trimmed_RC.fastq &&
    bowtie2 -p 20 -t --very-sensitive -x ${DmelanogasterIndex} -U ${INpath}${INfileprefix}_trimmed_RC.fastq | samtools view -@ 24 -bS | samtools sort -O bam -o ${INpath}${INfileprefix}_trimmed_RC_sort.bam &&
    samtools view -@ 20 -b -f 4 -o ${INpath}${INfileprefix}_trimmed_RC_sort_unmapped2DM.bam ${INpath}${INfileprefix}_trimmed_RC_sort.bam &&                      
    samtools fastq ${INpath}${INfileprefix}_trimmed_RC_sort_unmapped.bam > ${INpath}${INfileprefix}_trimmed_RC_sort_unmapped2DM.fastq &&
    bowtie2 -p 20 -t -k 100 -x ${HumanCHM13v1.1Index} -U ${INpath}${INfileprefix}_trimmed_RC_sort_unmapped2DM.fastq | samtools view -@ 24 -bS | samtools sort -O bam -o ${OUTpath}${INfileprefix}_trimmed_RC_sort_unmapped2DM_mapped2chm13v1.1_sort.bam &&
    bedtools bamtobed -i ${OUTpath}${INfileprefix}_trimmed_RC_sort_unmapped2DM_mapped2chm13v1.1_sort.bam > ${OUTpath}${INfileprefix}_trimmed_RC_sort_unmapped2DM_mapped2chm13v1.1_sort.bed
